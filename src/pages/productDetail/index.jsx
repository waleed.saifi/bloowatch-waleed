import React, { useState } from "react";
import Header from "../../components/header";
import {
  Typography,
  Box,
  Container,
  Grid,
  Button,
  ButtonGroup,
} from "@mui/material";
import KeyboardArrowUpIcon from "@mui/icons-material/KeyboardArrowUp";
import KeyboardArrowDownIcon from "@mui/icons-material/KeyboardArrowDown";
import { Related_Products } from "../../data/data";
import Card from "../../components/card";
import { useDispatch, useSelector } from "react-redux";
import { useNavigate } from "react-router-dom";
import { addToCart } from "../../redux/actions/productAction";

export default function Index() {
  const [counter, setCounter] = useState(1);
  const navigate = useNavigate();
  const dispatch = useDispatch();
  const { products } = useSelector((state) => state);
  const { item } = products;

  const handleDecrement = () => {
    if (counter > 1) {
      setCounter(counter - 1);
    }
  };
  const buttons = [
    <Button
      onClick={() => setCounter(counter + 1)}
      key="one"
      sx={{ color: "black" }}
    >
      <KeyboardArrowUpIcon />
    </Button>,
    <Button onClick={handleDecrement} key="two" sx={{ color: "black" }}>
      <KeyboardArrowDownIcon />
    </Button>,
  ];

  const handleAddToCart = (item, qtn) => {
    if (products?.cart?.length > 0) {
      for (let i = 0; i < products.cart.length; i++) {
        if (products.cart[i].id === item.id) {
          alert("Product already added");
          i = products.cart.length;
        } else if (i === products.cart.length - 1) {
          dispatch(addToCart(item, qtn));
          i = products.cart.length;
        }
      }
    } else {
      dispatch(addToCart(item, qtn));
    }
  };

  return (
    <div>
      <Header />
      <Box className="main_banner">
        <Container>
          <Typography className="banner_title">{item.name}</Typography>
        </Container>
      </Box>

      <Container>
        <Grid container mt={12} spacing={3}>
          <Grid item xs={12} md={6} lg={6}>
            <img src={item.imgUrl} width="100%" height="580px" />
          </Grid>
          <Grid item xs={12} md={6} lg={6} mt={3}>
            <Typography variant="h6">{item.name}</Typography>

            <Typography className="price_tag" my={2}>
              $ {item.price}
            </Typography>
            <Typography variant="caption">{item.description}</Typography>

            <Box
              sx={{
                display: "flex",
                alignItems: "center",
                mt: 3,
              }}
            >
              <b> {counter} </b>
              <ButtonGroup
                orientation="vertical"
                aria-label="vertical contained button group"
                variant="text"
                sx={{ ml: 3 }}
              >
                {buttons}
              </ButtonGroup>
              <Button
                sx={{ ml: 3, color: "black", borderColor: "black" }}
                variant="outlined"
                onClick={() => handleAddToCart(item, counter)}
              >
                Add to Cart
              </Button>
            </Box>
            <Box mt={3} display="flex" flexDirection="column">
              <Typography variant="caption" mt={1}>
                <b>SKU:</b> 003
              </Typography>
              <Typography variant="caption" mt={1}>
                <b>Categories:</b> {item.type}
              </Typography>
              <Typography variant="caption" mt={1}>
                <b>Tags:</b> {item.type}
              </Typography>
            </Box>
          </Grid>
        </Grid>

        <Typography variant="h6" my={3}>
          Description
        </Typography>
        <Typography variant="caption" mt={3}>
          {item.Details}
        </Typography>
        <Grid container mt={12} justifyContent="space-evenly">
          {Related_Products.map((data) => (
            <Grid item xs={12} md={6} lg={4}>
              <Card data={data} />
            </Grid>
          ))}
        </Grid>
      </Container>
    </div>
  );
}
