import React, { useState } from "react";
import Header from "../../components/header";
import {
  Typography,
  Box,
  Container,
  Grid,
  Button,
  ButtonGroup,
  Divider,
} from "@mui/material";
import { useSelector } from "react-redux";
import Table from "../../components/table";

export default function Index() {
  const { products } = useSelector((state) => state);
  const { cart } = products;
  const [subTotal, setSubTotal] = useState(0);

  return (
    <div>
      <Header />
      <Box className="main_banner">
        <Container>
          <Typography className="banner_title">Cart</Typography>
        </Container>
      </Box>

      <Container>
        <Box mt={15}>
          <Table data={cart} setSubTotal={setSubTotal} />
        </Box>
        <Box display="flex" mt={3} ml={10}>
          <Button className="blue_btn">Apply Coupon</Button>
          <Button className="blue_btn" sx={{ ml: 2 }}>
            Update Cart
          </Button>
        </Box>

        <Box my={10}>
          <Typography variant="h4" fontWeight="bold">
            CART TOTAL
          </Typography>
          <Box my={2} justifyContent="flex-start">
            <Typography variant="caption" fontWeight="bold">
              SUBTOTAL
            </Typography>
            <Typography variant="caption" color="gray" ml={12}>
              ${subTotal}
            </Typography>
            <Divider sx={{ width: "320px", mt: 2 }} />
          </Box>
          <Box my={2} justifyContent="flex-start">
            <Typography variant="caption" fontWeight="bold">
              SHIPPING
            </Typography>
            <Typography variant="caption" color="gray" ml={13}>
              Enter your address to view
            </Typography>
            <Divider sx={{ width: "320px", mt: 2 }} />
          </Box>
          <Box my={2} justifyContent="flex-start">
            <Typography variant="caption" fontWeight="bold">
              TOTAL
            </Typography>
            <Typography variant="caption" color="gray" ml={15}>
              ${subTotal}
            </Typography>
            <Divider sx={{ width: "320px", mt: 2 }} />
          </Box>
          <Button className="blue_btn">Proceed to Checkout</Button>
        </Box>
      </Container>
    </div>
  );
}
