import React, { useState, useEffect } from "react";
import Header from "../../components/header";
import { Typography, Box, Container, Grid, Button } from "@mui/material";
import { Products, Related_Products } from "../../data/data";
import Card from "../../components/card";
import Slider from "../../components/slider";
import SmallCard from "../../components/card/smallCard";
import { getProducts } from "../../redux/actions/productAction";
import { useDispatch, useSelector } from "react-redux";
import Footer from "../../components/footer";

export default function Index() {
  const categories = ["Shirts", "Sneakers", "Bags", "Watch", "Kefiah"];
  const [allProducts, setAllProducts] = useState([]);
  const dispatch = useDispatch();
  const { products } = useSelector((state) => state);
  const [value, setValue] = React.useState([0, 100]);
  const [selectedIndex, setSelectedIndex] = useState();

  useEffect(() => {
    dispatch(getProducts(Products));
  }, []);

  useEffect(() => {
    setAllProducts(products.items);
  }, [products]);

  const handleChange = (event, newValue) => {
    setValue(newValue);

    if (newValue[0] === 0 && newValue[1] === 100) {
      setAllProducts(products.items);
    } else {
      const RangedData = products?.items.filter(
        (item) => item.price > newValue[0] && item.price < newValue[1]
      );

      setAllProducts(RangedData);
    }
  };

  const handleSearch = (e) => {
    if (e) {
      const filteredData = products?.items.filter((item) =>
        item.name.toLowerCase().includes(e.toLowerCase())
      );
      setAllProducts(filteredData);
    } else {
      setAllProducts(products.items);
    }
  };

  const handleCategories = (data, index) => {
    setSelectedIndex(index);

    const filteredData = products?.items.filter((item) => item.type === data);
    setAllProducts(filteredData);
  };

  const handleDefault = () => {
    setAllProducts(products.items);
  };

  return (
    <div>
      <Header />
      <Box className="main_banner">
        <Container>
          <Typography className="banner_title">CLOTHING</Typography>
        </Container>
      </Box>

      <Container>
        <Button
          variant="outlined"
          onClick={() => handleDefault()}
          sx={{ my: 8 }}
        >
          Default Sorting
        </Button>
        <Grid container spacing={3} mb={8}>
          <Grid item container spacing={3} xs={12} sm={12} md={9} lg={9}>
            {allProducts?.length > 0 ? (
              <>
                {allProducts?.map((data) => (
                  <Grid item xs={12} md={6} lg={4}>
                    <Card data={data} />
                  </Grid>
                ))}
              </>
            ) : (
              <Grid
                container
                style={{
                  display: "flex",
                  justifyContent: "center",
                  alignItems: "center",
                }}
              >
                <Grid item>
                  <Typography variant="h6">No Product Found!</Typography>
                </Grid>
              </Grid>
            )}
          </Grid>
          <Grid item xs={12} sm={12} md={3} lg={3} p={1}>
            <Box>
              <Typography variant="body1">SEARCH</Typography>
              <input
                className="search_feild"
                placeholder="Search for a product"
                onChange={(e) => handleSearch(e.target.value)}
              />
            </Box>
            <Box my={5}>
              <Typography variant="body1">PRICE</Typography>
              <Slider value={value} handleChange={handleChange} />
              <Typography variant="caption">
                <b>
                  {" "}
                  PRICE: ${value[0]}--${value[1]}{" "}
                </b>
              </Typography>
            </Box>
            <Box>
              <Typography variant="body1">CATEGORIES</Typography>

              {categories.map((data, index) => (
                <Typography
                  mt={1.5}
                  sx={{
                    color: "gray",
                    cursor: "pointer",
                    fontWeight: selectedIndex === index ? "bold" : "normal",
                  }}
                  onClick={() => handleCategories(data, index)}
                >
                  {data}
                </Typography>
              ))}
            </Box>
            <Box my={5}>
              <Typography variant="body1">RELATED PRODUCTS</Typography>
              {Related_Products.map((data) => (
                <Box mt={4}>
                  <SmallCard data={data} />
                </Box>
              ))}
            </Box>
          </Grid>
        </Grid>
      </Container>
      <Footer />
    </div>
  );
}
