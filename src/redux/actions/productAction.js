export const PROUDUCT_TYPES = {
  GET_PRODUCTS: "GET_PRODUCTS",
  GET_PRODUCT: "GET_PRODUCT",
  ADD_TO_CART: "ADD_TO_CART",
  REMOVE_FROM_CART: "REMOVE_FROM_CART",
};

export const getProducts = (data) => async (dispatch) => {
  try {
    dispatch({
      type: PROUDUCT_TYPES.GET_PRODUCTS,
      payload: data,
    });
    return data;
  } catch (err) {}
};

export const getProduct = (data) => async (dispatch) => {
  try {
    dispatch({
      type: PROUDUCT_TYPES.GET_PRODUCT,
      payload: data,
    });
  } catch (err) {}
};

export const addToCart = (data, qtn) => async (dispatch) => {
  try {
    dispatch({
      type: PROUDUCT_TYPES.ADD_TO_CART,
      payload: { data, qtn },
    });
  } catch (err) {}
};
export const removeFromCart = (data) => async (dispatch) => {
  try {
    dispatch({
      type: PROUDUCT_TYPES.REMOVE_FROM_CART,
      payload: data,
    });
  } catch (err) {}
};
