import { PROUDUCT_TYPES } from "../actions/productAction";

const initialState = {
  items: [], //all products
  item: {}, //single product
  cart: [],
};

const productReducer = (state = initialState, action) => {
  switch (action.type) {
    case PROUDUCT_TYPES.GET_PRODUCTS:
      return {
        ...state,
        items: action.payload,
      };
    case PROUDUCT_TYPES.GET_PRODUCT:
      return {
        ...state,
        item: action.payload,
      };
    case PROUDUCT_TYPES.ADD_TO_CART:
      const new_obj = { ...action.payload.data, quantity: action.payload.qtn };

      return {
        ...state,
        cart: [new_obj, ...state.cart],
      };
    case PROUDUCT_TYPES.REMOVE_FROM_CART:
      const updatedArray = state.cart.filter(
        (element) => element.id !== action.payload.id
      );

      return {
        ...state,
        cart: updatedArray,
      };

    default: // need this for default case
      return state;
  }
};
export default productReducer;
