import React from "react";
import {
  Box,
  Table,
  TableBody,
  TableCell,
  TableContainer,
  TableHead,
  TableRow,
} from "@mui/material";
import CloseIcon from "@mui/icons-material/Close";
import { removeFromCart } from "../../redux/actions/productAction";
import { useDispatch } from "react-redux";

export default function Index({ data, setSubTotal }) {
  const dispatch = useDispatch();

  const handleRemove = (data) => {
    dispatch(removeFromCart(data));
  };

  let sum = 0;
  for (let i = 0; i < data.length; i++) {
    sum += data[i].price * data[i].quantity;
  }
  setSubTotal(sum);

  return (
    <div>
      <TableContainer>
        <Table>
          <TableHead sx={{ backgroundColor: "lightgray" }}>
            <TableRow>
              <TableCell></TableCell>
              <TableCell>
                <b>PRODUCT</b>
              </TableCell>
              <TableCell>
                {" "}
                <b>PRICE</b>
              </TableCell>
              <TableCell>
                {" "}
                <b>QUANTITY</b>
              </TableCell>
              <TableCell>
                {" "}
                <b>SUBTOTAL</b>
              </TableCell>
            </TableRow>
          </TableHead>
          {data.length > 0 ? (
            <>
              <TableBody>
                {data?.map((x) => (
                  <TableRow hover>
                    <TableCell>
                      <CloseIcon
                        sx={{ cursor: "pointer" }}
                        onClick={() => handleRemove(x)}
                      />
                    </TableCell>
                    <TableCell>
                      <Box sx={{ display: "flex", alignItems: "center" }}>
                        <img src={x.imgUrl} height="40px" width="35px" />
                        {x.name}
                      </Box>
                    </TableCell>
                    <TableCell> $ {x.price}</TableCell>
                    <TableCell> {x.quantity}</TableCell>
                    <TableCell> $ {x.price * x.quantity}</TableCell>
                  </TableRow>
                ))}
              </TableBody>
            </>
          ) : (
            <Box>
              <p>Cart is empty !</p>
            </Box>
          )}
        </Table>
      </TableContainer>
    </div>
  );
}
