import React from "react";
import { Typography, Box, Grid } from "@mui/material";
import { Related_Products } from "../../data/data";

export default function index() {
  return (
    <Box className="footer_main">
      <Grid container spacing={2} justifyContent="space-between">
        <Grid item xs={12} md={2.5} lg={2.5}>
          <Typography className="footer_title">ABOUT</Typography>

          <p className="footer_sub">
            Bloowatch is specialized software for watersports schools (surfing ,
            sailing , and diving) and outdoor activity provides.
          </p>
        </Grid>
        <Grid item xs={12} md={2} lg={2}>
          <Typography className="footer_title">CONTACT</Typography>
          <p className="footer_sub">09-0078-601</p>
          <p className="footer_sub">wave@gmail.com</p>
        </Grid>
        <Grid item xs={12} md={3} lg={3}>
          <Typography className="footer_title">USEFULL LINKS</Typography>
          <p className="footer_sub">About us</p>
          <p className="footer_sub">History</p>
        </Grid>
        <Grid item xs={12} md={3} lg={3}>
          <Typography className="footer_title">INSTAGRAM</Typography>
          {Related_Products.map((data) => (
            <img
              src={data.imgUrl}
              width="40px"
              height="40px"
              style={{ marginLeft: "5px", marginTop: "10px" }}
            />
          ))}
        </Grid>
      </Grid>
    </Box>
  );
}
