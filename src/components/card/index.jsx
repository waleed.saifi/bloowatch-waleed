import React from "react";
import { Typography, Box } from "@mui/material";
import { useNavigate } from "react-router-dom";
import { useDispatch } from "react-redux";
import { getProduct } from "../../redux/actions/productAction";

export default function Index({ data }) {
  const navigate = useNavigate();
  const dispatch = useDispatch();

  const handleNaviagte = async (data) => {
    await dispatch(getProduct(data));
    navigate("/productDetail");
  };

  return (
    <Box className="card_main" onClick={() => handleNaviagte(data)}>
      <img src={data.imgUrl} alt="card_img" width="100%" />
      <Box my={1}>
        <Typography>{data.name}</Typography>
        <p style={{ color: "blue", fontSize: "14px" }}>{data.type}</p>
      </Box>

      <Box className="price_box" mb={2}>
        <Typography className="price_tag">${data.price}</Typography>
      </Box>
    </Box>
  );
}
