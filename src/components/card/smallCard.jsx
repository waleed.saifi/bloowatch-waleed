import React from "react";
import { Typography, Box } from "@mui/material";
import Rating from "@mui/material/Rating";

export default function SmallCard({ data }) {
  const [value, setValue] = React.useState(4);
  return (
    <Box className="small_card">
      <Box>
        <img
          className="scard_img"
          src={data.imgUrl}
          width="60px"
          height="80px"
        />
      </Box>
      <Box ml={2} sx={{ display: "grid" }}>
        <Typography variant="caption">
          <b>{data.name}</b>
        </Typography>
        <Rating name="read-only" value={value} readOnly size="small" />
      </Box>
    </Box>
  );
}
